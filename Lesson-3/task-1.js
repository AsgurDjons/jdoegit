/**
 * Задача 1.
 *
 * Дописать требуемый код что бы код работал правильно.
 * Необходимо преобразовать первый символ переданной строки в заглавный.
 *
 * Условия:
 * - Необходимо проверить что параметр str является строкой
 */

function upperCaseFirst(str) {
    // str ← строка которая в нашем случае равна 'pitter' или ''
    // РЕШЕНИЕ НАЧАЛО
    let result = str;
    if (typeof result === "string" && str.length > 0) {
        result = result[0].toUpperCase() + result.slice(1, result.length);
    }else {
        result = '';
    }
    // РЕШЕНИЕ КОНЕЦ
    return result
}

console.log(upperCaseFirst('pitter')); // Pitter/
console.log(upperCaseFirst('')); // ''//
