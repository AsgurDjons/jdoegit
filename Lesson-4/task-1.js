/**
 * Задача 1.
 *
 * Вручную создать имплементацию функции `filter`.
 * Логика работы ручной имплементации должна быть такой-же,
 * как и у встроенного метода.
 *
 * Заметки:
 * - Встроенный метод Array.prototype.filter использовать запрещено.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - В качестве второго аргумента была передана не функция.
 *
 * Заметки:
 * - Второй аргумент встроенного метода filter (thisArg) имплементировать не нужно.
 */

 const array = ['Доброе утро!', 'Добрый вечер!', 3, 512, '#', 'До свидания!'];

 // Решение
function filter(array, func) {
     const newArr = [];
    if (Array.isArray(array) && typeof func === 'function') {
        for (let i = 0; i < array.length; i++) {
            const element = array[i];
            const isValid = func(element, i, array);
    
            if(isValid) {
                newArr.push(element);
            }
       }
       return newArr;
    }
    throw new Error('message');
}
 
 
 const filteredArray = filter(array, function (element, index, arrayRef) {
    //  console.log(`${index}:`, element, arrayRef);
 
     return element === 'Добрый вечер!';
 });
 
 console.log(filteredArray); // ['Добрый вечер!']