/**
 * Задача 2.
 *
 * Вручную создать имплементацию функции `reduce`.
 * Логика работы ручной имплементации должна быть такой-же,
 * как и у встроенного метода.
 *
 * Заметки:
 * - Встроенные методы Array.prototype.reduce и Array.prototype.reduceRight использовать запрещено;
 * - Третий аргумент функции reduce является не обязательным;
 * - Если третий аргумент передан — он станет начальным значением аккумулятора;
 * - Если третий аргумент не передан — начальным значением аккумулятора станет первый элемент обрабатываемого массива.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - В качестве второго аргумента была передана не функция;
 */

 const array = [1, 2, 3, 4, 5];
 const INITIAL_ACCUMULATOR = 6;
 
function reduce (array, fun, accumInit) {
    let sum;
    if (Array.isArray(array) && typeof accumInit === 'number') {
        sum = accumInit;
        array.forEach(item => {
            sum = sum + item;
        })
    }
    if (Array.isArray(array) && typeof accumInit === 'undefined') {
        sum = fun;
        array.forEach(item => {
            sum = sum + item;
        })
    }
    return sum;
}
 
 const result = reduce(
     array,
     function (accumulator, element, index, arrayRef) {
         console.log(`${index}:`, accumulator, element, arrayRef);

         return accumulator + element;
     },
     INITIAL_ACCUMULATOR,
 );
 
 console.log(result); // 21